package com.adamstyrc.workout.controllers;

import android.content.Context;
import android.os.Handler;

import com.adamstyrc.workout.model.Category;
import com.adamstyrc.workout.model.Exercise;
import com.adamstyrc.workout.ui.activity.MainActivity;
import com.adamstyrc.workout.ui.activity.ProgressActivity;
import com.adamstyrc.workout.ui.activity.WorkoutActivity;
import com.adamstyrc.workout.utils.FileManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by adamstyrc on 30/12/15.
 */
public class WorkoutChooserController {

    private static final int WORKOUT_EXERCISE_COUNT = 3;

    private static WorkoutChooserController instance;

    private Context applicationContext;
    private Category selectedCategory;
    private List<Exercise> exerciseList;
    private FileManager fileManager;

    private final Handler handler;
    private Runnable workoutPreparedChecker;

    public static void init(Context context) {
        instance = new WorkoutChooserController(context);
    }

    public static synchronized WorkoutChooserController getInstance() {
        return instance;
    }

    private WorkoutChooserController(Context context) {
        applicationContext = context.getApplicationContext();
        handler = new Handler(applicationContext.getMainLooper());
        fileManager = new FileManager(applicationContext);
        exerciseList = new ArrayList<>();
    }

    public void clear() {
        instance = null;
    }

    public void onCategorySelected(Category category) {
        selectedCategory = category;

        ExecutorService executor = Executors.newFixedThreadPool(3);
        List<Exercise> exercisesFromCategory = ExerciseController.getInstance().getExercises(selectedCategory);
        while (exerciseList.size() < WORKOUT_EXERCISE_COUNT) {
            addRandomExercise(exercisesFromCategory);
        }

        for (final Exercise chosenExercise : exerciseList) {
            if (!fileManager.isFileDownloaded(chosenExercise)) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            fileManager.download(chosenExercise.getVideoUrl());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private void addRandomExercise(List<Exercise> exercisesFromCategory) {
        int randomIndex = (int) (Math.random() * exercisesFromCategory.size());
        final Exercise chosenExercise = exercisesFromCategory.get(randomIndex);
        exercisesFromCategory.remove(randomIndex);

        if (chosenExercise.isTwin()) {
            if (exerciseList.size() == 2) {
                return;
            } else {
                List<Exercise> twinExercises = chosenExercise.getTwins();
                exerciseList.addAll(twinExercises);
            }
        } else {
            exerciseList.add(chosenExercise);
        }
    }

    public Exercise getExercise(int i) {
        return exerciseList.get(i);
    }

//    public int[] getExerciseIndicesArray() {
//        List<Exercise> allExercises = ExerciseController.getInstance().getExercises();
//        int[] exerciseIndices = new int[exerciseList.size()];
//
//        for (int i = 0; i < exerciseList.size(); i++) {
//            exerciseIndices[i] = allExercises.indexOf(exerciseList.get(i));
//        }
//
//        return exerciseIndices;
//    }

    public void startWorkout(final MainActivity activity) {
        workoutPreparedChecker = new Runnable() {
            @Override
            public void run() {
                for (Exercise exercise : exerciseList) {
                    if (!fileManager.isFileDownloaded(exercise)) {
                        ProgressActivity.show(activity);
                        handler.postDelayed(workoutPreparedChecker, 500);
                        return;
                    }
                }

                handler.removeCallbacks(workoutPreparedChecker);
                WorkoutController.init(exerciseList);
                WorkoutActivity.startVideoActivity(activity);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ProgressActivity.hide();
                        activity.showMainMenuFragment();

                    }
                }, 1000);
            }
        };

        workoutPreparedChecker.run();
    }
}
