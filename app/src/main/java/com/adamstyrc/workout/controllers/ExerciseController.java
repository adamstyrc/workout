package com.adamstyrc.workout.controllers;

import android.content.Context;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.model.Category;
import com.adamstyrc.workout.model.Exercise;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adamstyrc on 30/11/15.
 */
public class ExerciseController {

    private static ExerciseController instance = new ExerciseController();
    private List<Exercise> exercises;

    public void init(Context context) throws Exception {
        if (exercises != null) {
            return;
        }

        exercises = new ArrayList<>();
        String jsonString = readFromResources(context);

        JSONArray exercisesJsonArray = new JSONArray(jsonString);
        for (int i = 0; i < exercisesJsonArray.length(); i++) {
            JSONObject exerciseJson = exercisesJsonArray.getJSONObject(i);
            exercises.add(new Exercise(exerciseJson));
        }
    }

    public synchronized static ExerciseController getInstance() {
        return instance;
    }

    private ExerciseController() {
    }


    public List<Exercise> getExercises() {
        return exercises;
    }

    public List<Exercise> getExercises(Category category) {
        List<Exercise> categoryExercises = new ArrayList<>();
        for (Exercise exercise : exercises) {
            if (category.getId() == exercise.getType())
            categoryExercises.add(exercise);
        }
        return categoryExercises;
    }

    private String readFromResources(Context context) throws IOException {
        InputStream inputStream = context.getResources().openRawResource(R.raw.exercises);

        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
            inputStream.close();
        }

        return writer.toString();
    }
}
