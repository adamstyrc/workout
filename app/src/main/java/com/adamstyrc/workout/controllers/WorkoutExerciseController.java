package com.adamstyrc.workout.controllers;

import com.adamstyrc.workout.model.Exercise;

/**
 * Created by adamstyrc on 06/01/16.
 */
public class WorkoutExerciseController {

    private Exercise exercise;
    private int seriesCount;

    private int currentSeries = 1;

    public WorkoutExerciseController(Exercise exercise, int seriesCount) {
        this.exercise = exercise;
        this.seriesCount = seriesCount;
    }

    public void finishSeries() {
        currentSeries++;
    }

    public boolean isExerciseFinished() {
        return currentSeries > seriesCount;
    }

    public boolean isLastSeries() {
        return currentSeries == seriesCount;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public int getSeriesCount() {
        return seriesCount;
    }

    public int getCurrentSeries() {
        return currentSeries;
    }
}
