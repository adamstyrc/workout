package com.adamstyrc.workout.controllers;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.IOException;

/**
 * Created by adamstyrc on 10/01/16.
 */
public class WorkoutAudioController {

    private static final int WORKOUT_BEEPS_NUMBER = 1;
    private static final int BREAK_BEEPS_NUMBER = 1;

    private final Context context;

    private MediaPlayer workoutBeepPlayer;
    private MediaPlayer breakBeepPlayer;

    private boolean[] workoutBeepSoundsPlayed = new boolean[WORKOUT_BEEPS_NUMBER];
    private boolean[] breakBeepSoundsPlayed = new boolean[BREAK_BEEPS_NUMBER];

    public WorkoutAudioController(Context context) {
        this.context = context;

        workoutBeepPlayer = new MediaPlayer();
        Uri parse = Uri.parse("http://www.soundjay.com/button/beep-22.mp3");
        try {
            workoutBeepPlayer.setDataSource(context, parse);
            workoutBeepPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        breakBeepPlayer = new MediaPlayer();
        parse = Uri.parse("http://www.soundjay.com/button/beep-23.mp3");
        try {
            breakBeepPlayer.setDataSource(context, parse);
            breakBeepPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playWorkoutBeepIfNeeded(int secondsLeft) {
        if (secondsLeft < WORKOUT_BEEPS_NUMBER && secondsLeft >= 0) {
            if (!workoutBeepSoundsPlayed[secondsLeft]) {
                workoutBeepPlayer.start();
                workoutBeepSoundsPlayed[secondsLeft] = true;
            }
        }
    }

    public void playBreakBeepIfNeeded(int secondsLeft) {
        if (secondsLeft < BREAK_BEEPS_NUMBER && secondsLeft >= 0) {
            if (!breakBeepSoundsPlayed[secondsLeft]) {
                breakBeepPlayer.start();
                breakBeepSoundsPlayed[secondsLeft] = true;
            }
        }
    }

    public void clearWorkoutBeeps() {
        for (int i = 0; i < workoutBeepSoundsPlayed.length; i++) {
            workoutBeepSoundsPlayed[i] = false;
        }
    }

    public void clearBreakBeeps() {
        for (int i = 0; i < breakBeepSoundsPlayed.length; i++) {
            breakBeepSoundsPlayed[i] = false;
        }
    }

    public int getWorkoutBeepsNumber() {
        return WORKOUT_BEEPS_NUMBER;
    }

    public int getBreakBeepsNumber() {
        return BREAK_BEEPS_NUMBER;
    }
}
