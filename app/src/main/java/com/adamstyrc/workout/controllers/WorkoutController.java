package com.adamstyrc.workout.controllers;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import com.adamstyrc.workout.BuildConfig;
import com.adamstyrc.workout.model.Exercise;
import com.adamstyrc.workout.ui.CountdownTimerLayout;
import com.adamstyrc.workout.ui.activity.WorkoutActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adamstyrc on 18/12/15.
 */
public class WorkoutController {

    private static final long BREAK_TIME_IN_MILLIS = BuildConfig.BREAK_DURATION;
    private static final int SERIES_COUNT = BuildConfig.SERIES_COUNT;
    private static final int SECONDS_BEFORE_TRAILER_SHOW = 3;

    private static WorkoutController instance;

    private List<WorkoutExerciseController> exerciseControllers;
    private  WorkoutAudioController workoutAudioController;
    private int currentExerciseControllerIndex;

    private long millisLeft;
    private boolean paused = true;

    private WorkoutActivity activity;
    private CountdownTimerLayout timerLayout;
    private VideoView videoView;
    private CountDownTimer timer;
    private Handler handler = new Handler();
    private Runnable timerStartChecker;
    private boolean breakTime;
    private TextView tvTitle;
    private CountDownTimer breakTimer;
    private int totalExerciseSeconds;


    public static WorkoutController getInstance() {
        return instance;
    }

    public static void init(List<Exercise> exercises) {
        instance = new WorkoutController(exercises);
    }

    private WorkoutController(List<Exercise> exercises) {
        exerciseControllers = new ArrayList<>();

        for (Exercise exercise : exercises) {
            exerciseControllers.add(new WorkoutExerciseController(exercise, SERIES_COUNT));
        }

        currentExerciseControllerIndex = 0;

        millisLeft = getCurrentExercise().getDuration();
    }


    public void prepare(Context context) {
        videoView.setVideoURI(getCurrentExercise().getUri(context));
        videoView.seekTo(100);

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paused = !paused;

                activity.setPauseView(paused);

                if (paused) {
                    videoView.pause();
                    stopTimer();
                } else {
                    videoView.start();
                    if (videoView.getCurrentPosition() >= getCurrentExercise().getStartDelay()) {
                        startTimer(millisLeft);
                    }
                }
            }
        });

        WorkoutExerciseController currentExerciseController = getCurrentExerciseController();
        tvTitle.setText(getCurrentExercise().getName() + " " + currentExerciseController.getCurrentSeries() + " / " + currentExerciseController.getSeriesCount());

        totalExerciseSeconds = getSecondsLeft();
        timerLayout.setTotalSeconds(totalExerciseSeconds);

        launchTimerStartChecker();
    }

    public void start() {
        paused = false;

        activity.setPauseView(paused);

        videoView.start();
        if (videoView.getCurrentPosition() >= getCurrentExercise().getStartDelay()) {
            startTimer(millisLeft);
        }
    }

    public void onDestroy() {
        activity = null;
        timerLayout = null;
        videoView = null;

        stopTimer();
        stopBreakTimer();


        handler.removeCallbacks(timerStartChecker);

        workoutAudioController = null;
        instance = null;
    }

    public void setVideoView(VideoView videoView) {
        this.videoView = videoView;
    }

    public void setCountdownTimerLayout(CountdownTimerLayout countdownTimerLayout) {
        timerLayout = countdownTimerLayout;
    }

    private void launchTimerStartChecker() {
        timerStartChecker = new Runnable() {
            @Override
            public void run() {
                int currentVideoMillis = videoView.getCurrentPosition();
                if (currentVideoMillis >= getCurrentExercise().getStartDelay()) {
                    startTimer(millisLeft);
                } else {
                    handler.postDelayed(timerStartChecker, 200);
                }
            }
        };
        timerStartChecker.run();
    }

    private void startTimer(final long millis) {
        timer = new CountDownTimer(millis, 50) {

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                millisLeft = leftTimeInMilliseconds;

                timerLayout.setProgress(getSecondsLeft());


                boolean isNextExerciseExist = currentExerciseControllerIndex < exerciseControllers.size() - 1;
                boolean isLastSeriesOfCurrentExercise = getCurrentExerciseController().isLastSeries();
                if (leftTimeInMilliseconds <= (SECONDS_BEFORE_TRAILER_SHOW * 1000) && isNextExerciseExist && isLastSeriesOfCurrentExercise) {
                    if (!activity.isTrailerVisible()) {
                        WorkoutExerciseController currentExerciseController = exerciseControllers.get(currentExerciseControllerIndex + 1);
                        activity.showTrailer(currentExerciseController.getExercise());
                    }
                }

                int secondsLeft = getSecondsLeft();
                workoutAudioController.playWorkoutBeepIfNeeded(secondsLeft);
            }

            @Override
            public void onFinish() {
                onExerciseSeriesFinished();

                workoutAudioController.clearWorkoutBeeps();
            }
        };
        timer.start();
    }

    private void onExerciseSeriesFinished() {
        videoView.pause();

        getCurrentExerciseController().finishSeries();

        if (getCurrentExerciseController().isExerciseFinished()) {
            currentExerciseControllerIndex++;
        }

        if (currentExerciseControllerIndex < exerciseControllers.size()) {
            activity.showBreakScreen();
            activity.setBreakSecondsLeft((int)(BREAK_TIME_IN_MILLIS / 1000));
            breakTime = true;

            breakTimer = new CountDownTimer(BREAK_TIME_IN_MILLIS, 200) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int secondsLeft = ((int) millisUntilFinished / 1000);
                    int uiSecondsLeft = secondsLeft + 1;
                    activity.setBreakSecondsLeft(uiSecondsLeft);

                    workoutAudioController.playBreakBeepIfNeeded(secondsLeft);
                }

                @Override
                public void onFinish() {
//                    stopBreakTimer();
                    breakTime = false;
                    activity.hideBreakScreen();
                    activity.hideTrailer();
                    instance.start();

                    workoutAudioController.clearBreakBeeps();
                }
            }.start();

            millisLeft = getCurrentExercise().getDuration();
            prepare(activity);
        } else {
            activity.finish();
        }
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void stopBreakTimer() {
        if (breakTimer != null) {
            breakTimer.cancel();
            breakTimer = null;
        }
    }

    private WorkoutExerciseController getCurrentExerciseController() {
        return exerciseControllers.get(currentExerciseControllerIndex);
    }

    private Exercise getCurrentExercise() {
        return getCurrentExerciseController().getExercise();
    }

    private int getSecondsLeft() {
        return (int) millisLeft / 1000;
    }

    public void setTvTitle(TextView tvTitle) {
        this.tvTitle = tvTitle;
    }

    public void setActivity(WorkoutActivity activity) {
        this.activity = activity;

        workoutAudioController = new WorkoutAudioController(activity);
    }
}
