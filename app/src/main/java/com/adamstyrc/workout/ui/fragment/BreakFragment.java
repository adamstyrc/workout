package com.adamstyrc.workout.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.controllers.WorkoutController;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by adamstyrc on 23/12/15.
 */
public class BreakFragment extends Fragment {

    @Bind(R.id.tvBreakTimer)
    TextView tvBreakTimer;

    private int secondsLeft;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_break, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setBreakTimer(secondsLeft);
    }

    public void setSecondsLeft(int secondsLeft) {
        this.secondsLeft = secondsLeft;

        if (tvBreakTimer != null) {
            setBreakTimer(secondsLeft);
        }
    }

    private void setBreakTimer(int secondsLeft) {
        String text = "..." + secondsLeft + "...";
        tvBreakTimer.setText(text);
    }
}
