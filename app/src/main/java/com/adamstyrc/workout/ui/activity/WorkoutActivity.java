package com.adamstyrc.workout.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.analytics.GoogleAnalytics;
import com.adamstyrc.workout.controllers.WorkoutController;
import com.adamstyrc.workout.model.Exercise;
import com.adamstyrc.workout.ui.CountdownTimerLayout;
import com.adamstyrc.workout.ui.fragment.BreakFragment;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import butterknife.Bind;
import butterknife.ButterKnife;

public class WorkoutActivity extends Activity {

    private WorkoutController controller;

    @Bind(R.id.vVideo)
    VideoView videoView;
    @Bind((R.id.tvTitle))
    TextView tvTitle;
    @Bind((R.id.layoutTimer))
    CountdownTimerLayout layoutTimer;
    @Bind((R.id.ivPlay))
    ImageView ivPlay;
    @Bind((R.id.layoutShadowLayer))
    ViewGroup layoutShadowLayer;

    @Bind((R.id.layoutTrailer))
    ViewGroup layoutTrailer;
    @Bind((R.id.vVideoTrailer))
    VideoView vVideoTrailer;
    @Bind((R.id.tvTrailerTitle))
    TextView tvTrailerTitle;

    BreakFragment breakFragment;
    private AlertDialog exitDialog;


    public static void startVideoActivity(Context context) {
        Intent intent = new Intent(context, WorkoutActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);
        ButterKnife.bind(this);

        breakFragment = new BreakFragment();
        layoutTrailer.setVisibility(View.GONE);

        controller = WorkoutController.getInstance();
        controller.setActivity(this);
        controller.setVideoView(videoView);
        controller.setTvTitle(tvTitle);
        controller.setCountdownTimerLayout(layoutTimer);
        controller.prepare(this);
        controller.start();

        vVideoTrailer.setZOrderMediaOverlay(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = GoogleAnalytics.getDefaultTracker(this);
        tracker.setScreenName("WorkoutActivity");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void setPauseView(boolean visible) {
        ivPlay.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        layoutShadowLayer.setBackgroundResource(visible ? R.color.shadow_dark : android.R.color.transparent);
    }

    public void showBreakScreen() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.layoutShadowLayer, breakFragment);
        ft.commit();
    }

    public void setBreakSecondsLeft(int secondsLeft) {
        breakFragment.setSecondsLeft(secondsLeft);
    }

    public void hideBreakScreen() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(breakFragment);
        ft.commit();
    }

    public boolean isTrailerVisible() {
        return layoutTrailer.getVisibility() == View.VISIBLE;
    }

    public void showTrailer(final Exercise nextExercise) {
        layoutTrailer.setVisibility(View.VISIBLE);
        if (layoutTrailer.getParent() == null) {
            layoutShadowLayer.addView(layoutTrailer);
        }

        tvTrailerTitle.setText(nextExercise.getName());
        vVideoTrailer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                vVideoTrailer.start();
            }
        });
        vVideoTrailer.setVideoURI(nextExercise.getUri(this));
        vVideoTrailer.seekTo((int) nextExercise.getStartDelay());
    }

    public void hideTrailer() {
        vVideoTrailer.stopPlayback();

        layoutShadowLayer.removeView(layoutTrailer);
        layoutTrailer.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (exitDialog != null) {
            exitDialog.dismiss();
        }
        controller.onDestroy();
    }

    @Override
    public void onBackPressed() {
        exitDialog = new AlertDialog.Builder(this)
//                .setIcon(android.R.drawable.stat_notify_error)
                .setTitle(R.string.exit_dialog_title)
                .setMessage(R.string.exit_dialog_message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        WorkoutActivity.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        exitDialog.dismiss();
                        exitDialog = null;
                    }
                }).create();
        exitDialog.show();
    }
}
