/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.adamstyrc.workout.ui;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.Presenter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.controllers.ExerciseController;
import com.adamstyrc.workout.model.Exercise;
import com.adamstyrc.workout.ui.activity.ExerciseDetailsActivity;
import com.adamstyrc.workout.ui.activity.ProgressActivity;
import com.adamstyrc.workout.utils.FileManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.IOException;

//import com.bumptech.glide.Glide;

/*
 * A CardPresenter is used to generate Views and bind Objects to them on demand. 
 * It contains an Image CardView
 */
public class CardPresenter extends Presenter {
    private static final String TAG = "CardPresenter";

    private static int CARD_WIDTH = 313;
    private static int CARD_HEIGHT = 190;
    private static int sSelectedBackgroundColor;
    private static int sDefaultBackgroundColor;
    private Drawable mDefaultCardImage;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Log.d(TAG, "onCreateViewHolder");

        sDefaultBackgroundColor = parent.getResources().getColor(R.color.default_background);
        sSelectedBackgroundColor = parent.getResources().getColor(R.color.orange);
//        mDefaultCardImage = parent.getResources().getDrawable(R.drawable.movie);

        ImageCardView cardView = new ImageCardView(parent.getContext()) {
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };

        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        updateCardBackgroundColor(cardView, false);
        return new ViewHolder(cardView);
    }

    private static void updateCardBackgroundColor(ImageCardView view, boolean selected) {
        int color = selected ? sSelectedBackgroundColor : sDefaultBackgroundColor;
        // Both background colors should be set because the view's background is temporarily visible
        // during animations.
        view.setBackgroundColor(color);
        view.findViewById(R.id.info_field).setBackgroundColor(color);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        final Exercise exercise = (Exercise) item;
        ImageCardView cardView = (ImageCardView) viewHolder.view;

        Log.d(TAG, "onBindViewHolder");
//        if (exercise.getCardImageUrl() != null) {
            cardView.setTitleText(exercise.getName());
//            cardView.setContentText(exercise.getName());
            cardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);

        final Handler handler = new Handler(viewHolder.view.getContext().getMainLooper());

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final FileManager fileManager = new FileManager(v.getContext());
                if (!fileManager.isFileDownloaded(exercise)) {
                    ProgressActivity.show(v.getContext());
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                fileManager.download(exercise.getVideoUrl());

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        ProgressActivity.hide();

                                        int exerciseIndex = ExerciseController.getInstance().getExercises().indexOf(exercise);
                                        ExerciseDetailsActivity.startActivity(v.getContext(), exerciseIndex);
                                    }
                                });

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                } else {
                    int exerciseIndex = ExerciseController.getInstance().getExercises().indexOf(exercise);
                    ExerciseDetailsActivity.startActivity(v.getContext(), exerciseIndex);
                }
            }
        });

        Glide.with(viewHolder.view.getContext())
                .load(exercise.getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(mDefaultCardImage)
                .into(cardView.getMainImageView());
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        Log.d(TAG, "onUnbindViewHolder");
        ImageCardView cardView = (ImageCardView) viewHolder.view;
        // Remove references to images so that the garbage collector can free up memory
        cardView.setBadgeImage(null);
        cardView.setMainImage(null);
    }
}
