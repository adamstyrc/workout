package com.adamstyrc.workout.ui.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Activity;

import com.adamstyrc.workout.analytics.GoogleAnalytics;
import com.adamstyrc.workout.controllers.ExerciseController;
import com.adamstyrc.workout.R;
import com.adamstyrc.workout.ui.fragment.MainMenuFragment;
import com.adamstyrc.workout.ui.fragment.WorkoutChooserFragment;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends Activity {

    private MainMenuFragment mainMenuFragment;
    private WorkoutChooserFragment workoutChooserFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        try {
            ExerciseController.getInstance().init(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mainMenuFragment = new MainMenuFragment();
        showMainMenuFragment();


        GoogleAnalytics.getDefaultTracker(this)
                .enableAdvertisingIdCollection(true);
        Fabric.with(this, new Crashlytics());
    }

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = GoogleAnalytics.getDefaultTracker(this);
        tracker.setScreenName("MainActivity");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void showWorkoutChooserFragment() {
        workoutChooserFragment = new WorkoutChooserFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.layoutContainer, workoutChooserFragment);
        ft.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        ft.commit();
    }


    public void showMainMenuFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.layoutContainer, mainMenuFragment);
        ft.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        ft.commitAllowingStateLoss();
    }


    @Override
    public void onBackPressed() {
        if (mainMenuFragment.isAdded()) {
            super.onBackPressed();
        } else {
            showMainMenuFragment();
        }
    }
}
