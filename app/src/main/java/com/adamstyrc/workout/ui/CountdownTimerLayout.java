package com.adamstyrc.workout.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adamstyrc.workout.R;


/**
 * Created by adamstyrc on 18/01/16.
 */
public class CountdownTimerLayout extends FrameLayout {

    private ProgressBar pbTimer;
    private TextView tvTimer;

    public CountdownTimerLayout(Context context) {
        this(context, null, 0, 0);
    }

    public CountdownTimerLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public CountdownTimerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CountdownTimerLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        LayoutInflater.from(context).inflate(R.layout.layout_countdown_timer, this, true);

        pbTimer = (ProgressBar) findViewById(R.id.pbTimer);
        tvTimer = (TextView) findViewById(R.id.tvTimer);
    }

    public void setTotalSeconds(int totalSeconds) {
        tvTimer.setText(String.valueOf(totalSeconds));

        pbTimer.setMax(totalSeconds);
        pbTimer.setProgress(0);
    }

    public void setProgress(int secondsLeft) {
        pbTimer.setProgress(pbTimer.getMax() - secondsLeft);
        tvTimer.setText(String.valueOf(secondsLeft));
    }
}
