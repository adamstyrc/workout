package com.adamstyrc.workout.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.ui.activity.ExercisesActivity;
import com.adamstyrc.workout.ui.activity.MainActivity;
import com.adamstyrc.workout.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by adamstyrc on 20/12/15.
 */
public class MainMenuFragment extends Fragment {

    @Bind(R.id.btnWorkout)
    Button btnWorkout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        btnWorkout.requestFocus();
    }

    @OnClick(R.id.btnExercises)
    public void onBtnExercisesClicked() {
        ExercisesActivity.startExercisesActivity(getActivity());
    }

    @OnClick(R.id.btnWorkout)
    public void onBtnWorkoutClicked() {
        ((MainActivity) getActivity()).showWorkoutChooserFragment();

    }

    @OnClick(R.id.btnSettings)
    public void onBtnSettingsClicked() {
//        Utils.isSdcardAvailable();
        throw new NullPointerException();
    }
}
