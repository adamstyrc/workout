package com.adamstyrc.workout.ui.fragment;

import android.animation.Animator;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.controllers.WorkoutChooserController;
import com.adamstyrc.workout.model.Category;
import com.adamstyrc.workout.ui.activity.MainActivity;
import com.adamstyrc.workout.ui.activity.WorkoutActivity;
import com.adamstyrc.workout.utils.AnimationUtils;
import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by adamstyrc on 20/12/15.
 */
public class WorkoutChooserFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {

    @Bind(R.id.layoutRoot)
    ViewGroup layoutRoot;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.ibAbs)
    ImageButton ibAbs;
    @Bind(R.id.ibButt)
    ImageButton ibButt;
    @Bind(R.id.ibLegs)
    ImageButton ibLegs;
    @Bind(R.id.tv5Mins)
    TextView tv5Mins;

    @Bind(R.id.layoutExercises)
    View layoutExercises;
    @Bind(R.id.layoutExercise1)
    View layoutExercise1;
    @Bind(R.id.layoutExercise2)
    View layoutExercise2;
    @Bind(R.id.layoutExercise3)
    View layoutExercise3;
    @Bind(R.id.ivExercise1)
    ImageView ivExercise1;
    @Bind(R.id.ivExercise2)
    ImageView ivExercise2;
    @Bind(R.id.ivExercise3)
    ImageView ivExercise3;
    @Bind(R.id.tvExercise1)
    TextView tvExercise1;
    @Bind(R.id.tvExercise2)
    TextView tvExercise2;
    @Bind(R.id.tvExercise3)
    TextView tvExercise3;

    @Bind(R.id.tvReady)
    TextView tvReady;

    private View clickedBanner;
    private WorkoutChooserController controller;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WorkoutChooserController.init(getActivity());
        controller = WorkoutChooserController.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_workout_chooser, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ViewAnimationUtils.createCircularReveal(view, 960, 540, 0, 1000).setDuration(500).start();

        Glide.with(this)
                .load("http://qknvq17fubf27ogvn32k78jr.wpengine.netdna-cdn.com/wp-content/uploads/2015/06/shesfit-blog-banner-4.jpg")
                .centerCrop()
                .into(ibButt);

        Glide.with(this)
                .load("http://holmesplace.es/_public/files/6/5/65ca14_toned_legs_banner.jpg_960x400_c_.jpg")
                .centerCrop()
                .into(ibLegs);

        ibAbs.setOnClickListener(this);
        ibButt.setOnClickListener(this);
        ibLegs.setOnClickListener(this);
        tv5Mins.setOnClickListener(this);

        ibAbs.setOnFocusChangeListener(this);
        ibButt.setOnFocusChangeListener(this);
        ibLegs.setOnFocusChangeListener(this);
        tv5Mins.setOnFocusChangeListener(this);

        ibAbs.requestFocus();
    }

    private void updateBannerView(View bannerView) {
        if (bannerView.hasFocus()) {
            AnimationUtils.scale(bannerView, 1.0f, 1.1f, 300);
        } else {
            AnimationUtils.scale(bannerView, 1.1f, 1.0f, 300);
        }
    }

    @Override
    public void onClick(View clickedView) {
        switch (clickedView.getId()) {
            case R.id.ibAbs:
                controller.onCategorySelected(Category.ABS);
                onBannerClicked(clickedView);
                break;
            case R.id.ibButt:
                controller.onCategorySelected(Category.BUTT);
                onBannerClicked(clickedView);
                break;
            case R.id.ibLegs:
                controller.onCategorySelected(Category.LEGS);
                onBannerClicked(clickedView);
                break;
            case R.id.tv5Mins:
                controller.startWorkout((MainActivity)getActivity());

                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ibAbs:
            case R.id.ibButt:
            case R.id.ibLegs:
                updateBannerView(view);
                break;
            case R.id.tv5Mins:
                break;
        }
    }

    private void onBannerClicked(final View clickedView) {
        for (int i = 0; i < layoutRoot.getChildCount(); i++) {
            final View childView = layoutRoot.getChildAt(i);
            if (childView instanceof ImageButton && childView != clickedView) {
                removeWorkoutBanner(childView);
            }
        }

        tvTitle.setVisibility(View.GONE);

        Glide.with(getActivity())
                .load(controller.getExercise(0).getImageUrl())
                .centerCrop()
                .into(ivExercise1);
        tvExercise1.setText(controller.getExercise(0).getName());

        Glide.with(getActivity())
                .load(controller.getExercise(1).getImageUrl())
                .centerCrop()
                .into(ivExercise2);
        tvExercise2.setText(controller.getExercise(1).getName());

        Glide.with(getActivity())
                .load(controller.getExercise(2).getImageUrl())
                .centerCrop()
                .into(ivExercise3);
        tvExercise3.setText(controller.getExercise(2).getName());

        clickedBanner = clickedView;
        clickedView.setFocusable(false);
        clickedView.setAlpha(1.0f);

        clickedView.animate().y(1080 - ibAbs.getHeight() - 50).x(50).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                AnimationUtils.extendWidth(clickedView, 690, 280, 500, new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        rescaleBanners();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                tv5Mins.setVisibility(View.VISIBLE);
                tv5Mins.setAlpha(0.0f);
                tv5Mins.animate().alpha(1.0f).setStartDelay(500).setDuration(500).start();

                layoutExercises.setVisibility(View.VISIBLE);

                layoutExercise1.setAlpha(0.0f);
                layoutExercise2.setAlpha(0.0f);
                layoutExercise3.setAlpha(0.0f);
                layoutExercise1.animate().alpha(1.0f).setStartDelay(1500).setDuration(500).start();
                layoutExercise2.animate().alpha(1.0f).setStartDelay(1800).setDuration(500).start();
                layoutExercise3.animate().alpha(1.0f).setStartDelay(2100).setDuration(500).start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tvReady.setVisibility(View.VISIBLE);
                    }
                }, 3000);

                tv5Mins.requestFocus();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
    }

    private void rescaleBanners() {
        ibAbs.setImageResource(R.drawable.abs);
        ibButt.setImageResource(R.drawable.image_butt);
        ibLegs.setImageResource(R.drawable.image_legs);
    }


    private void removeWorkoutBanner(final View bannerView) {
        bannerView.setFocusable(false);
        bannerView.animate().alpha(0).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                bannerView.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {


            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        controller.clear();
    }
}
