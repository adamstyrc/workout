package com.adamstyrc.workout.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.VideoView;

import com.adamstyrc.workout.controllers.ExerciseController;
import com.adamstyrc.workout.R;
import com.adamstyrc.workout.model.Exercise;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by adamstyrc on 19/12/15.
 */
public class ExerciseDetailsActivity extends Activity {

    private static final String EXERCISE_INDEX = "EXERCISE_INDEX";
    private Exercise exercise;

    @Bind(R.id.vVideo)
    VideoView vVideo;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvDescription)
    TextView tvDescription;

    public static void startActivity(Context context, int exerciseIndex) {
        Intent intent = new Intent(context, ExerciseDetailsActivity.class);
        intent.putExtra(EXERCISE_INDEX, exerciseIndex);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_details);
        ButterKnife.bind(this);

        int exerciseIndex = getIntent().getIntExtra(EXERCISE_INDEX, -1);
        exercise = ExerciseController.getInstance().getExercises().get(exerciseIndex);

        tvTitle.setText(exercise.getName());
        tvDescription.setText(exercise.getDescription());

        vVideo.setVideoURI(exercise.getUri(this));
        vVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                vVideo.start();
            }
        });
    }
}
