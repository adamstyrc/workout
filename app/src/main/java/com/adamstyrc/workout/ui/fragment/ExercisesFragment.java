package com.adamstyrc.workout.ui.fragment;

import android.animation.Animator;
import android.content.Context;
import android.os.Bundle;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;

import com.adamstyrc.workout.ui.CardPresenter;
import com.adamstyrc.workout.R;
import com.adamstyrc.workout.controllers.ExerciseController;
import com.adamstyrc.workout.model.Category;
import com.adamstyrc.workout.model.Exercise;

import java.util.List;


/**
 * Created by adamstyrc on 30/11/15.
 */
public class ExercisesFragment extends BrowseFragment {

    private ArrayObjectAdapter mRowsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setTitle("Exercises");
        setBrandColor(getResources().getColor(R.color.fastlane_background));
        loadRows();
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                ViewAnimationUtils.createCircularReveal(view, 300, 850, 0, 1700).setDuration(500).start();
            }
        });
    }

    private void loadRows() {
        ExerciseController exerciseController = ExerciseController.getInstance();
        Category[] categories = Category.values();

        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        CardPresenter cardPresenter = new CardPresenter();

        for (int i = 0; i < categories.length; i++) {
            Category category = categories[i];

            HeaderItem header = new HeaderItem(i, category.name());
            ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(cardPresenter);
            List<Exercise> categoryExercises = exerciseController.getExercises(category);
            for (int j = 0; j < categoryExercises.size(); j++) {
                listRowAdapter.add(categoryExercises.get(j));
            }
            mRowsAdapter.add(new ListRow(header, listRowAdapter));
        }

        setAdapter(mRowsAdapter);
    }
}
