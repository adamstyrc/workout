package com.adamstyrc.workout.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.adamstyrc.workout.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by adamstyrc on 30/12/15.
 */
public class ProgressActivity extends Activity {

    private static Activity activity;
    private static boolean showed;

    @Bind(R.id.pbLoader)
    ProgressBar pbLoader;

    public static void show(Context context) {
        if (!showed) {
            Intent intent = new Intent(context, ProgressActivity.class);
            context.startActivity(intent);
            showed = true;
        }
    }

    public static void hide() {
        if (showed && activity != null) {
            activity.finish();
            activity = null;

            showed = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        ButterKnife.bind(this);

        pbLoader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.orange), PorterDuff.Mode.SRC_IN);

        if (showed) {
            activity = this;
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
    }
}
