package com.adamstyrc.workout.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.ViewAnimationUtils;

import com.adamstyrc.workout.R;
import com.adamstyrc.workout.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ExercisesActivity extends Activity {


    public static void startExercisesActivity(Context context) {
        Intent intent = new Intent(context, ExercisesActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Tracker tracker = GoogleAnalytics.getDefaultTracker(this);
        tracker.setScreenName("ExercisesActivity");
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
