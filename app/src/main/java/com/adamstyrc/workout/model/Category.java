package com.adamstyrc.workout.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adamstyrc on 19/12/15.
 */
public enum Category {
    ABS(0),
    BUTT(1),
    LEGS(2);

    private int id;

    Category(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
