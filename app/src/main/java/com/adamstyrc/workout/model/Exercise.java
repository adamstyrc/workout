package com.adamstyrc.workout.model;

import android.content.Context;
import android.net.Uri;

import com.adamstyrc.workout.BuildConfig;
import com.adamstyrc.workout.utils.FileManager;
import com.adamstyrc.workout.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adamstyrc on 30/11/15.
 */
public class Exercise {

    private String name;
    private int type;
    private String description;
    private String imageUrl;
    private String videoUrl;
    private String localVideoRawName;
    private long duration;
    private long startDelay;
    private JSONArray twins;
    private List<Exercise> twinsList;

    public Exercise(JSONObject jsonObject) {
        name = jsonObject.optString("name", null);
        type = jsonObject.optInt("type", -1);
        description = jsonObject.optString("description");
        imageUrl = jsonObject.optString("imageUrl", null);
        videoUrl = jsonObject.optString("videoUrl", null);
        localVideoRawName = jsonObject.optString("localVideoRawName", null);
        duration = jsonObject.optLong("duration");
        startDelay = jsonObject.optInt("startDelay");
        twins = jsonObject.optJSONArray("twins");
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        if (imageUrl != null) {
            return imageUrl;
        } else {
            return getTwins().get(0).getImageUrl();
        }
    }

    public long getDuration() {
        return BuildConfig.EXERCISE_DURATION;
    }

    public long getStartDelay() {
        return startDelay;
    }

    public String getLocalVideoRawName() {
        return localVideoRawName;
    }

    public String getVideoUrl() {
        if (videoUrl != null) {
            return videoUrl;
        } else {
            return getTwins().get(0).getVideoUrl();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Uri getUri(Context context) {
        String localVideoRawName = getLocalVideoRawName();
        if (localVideoRawName != null) {
            return Utils.getUriForRawResource(context, localVideoRawName);
        } else {
            return Uri.parse(new FileManager(context).getFilePath(getVideoUrl()));

        }
    }

    public boolean isTwin() {
        return twins != null;
    }

    public List<Exercise> getTwins() {
        if (twinsList == null) {
            twinsList = new ArrayList<>();

            for (int i = 0; i < twins.length(); i++) {
                try {
                    JSONObject siblingJSONObject = twins.getJSONObject(i);
                    Exercise exercise = new Exercise(siblingJSONObject);

                    String newName = this.name + " - " + exercise.getName();
                    exercise.setName(newName);
                    exercise.setType(type);
                    exercise.setDescription(description);

                    twinsList.add(exercise);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return twinsList;
    }
}
