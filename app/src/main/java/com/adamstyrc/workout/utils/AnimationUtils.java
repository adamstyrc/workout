package com.adamstyrc.workout.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

/**
 * Created by adamstyrc on 22/12/15.
 */
public class AnimationUtils {

    public static void scale(View view, float fromSize, float toSize, int duration) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(fromSize, toSize, fromSize, toSize, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        scaleAnimation.setDuration(duration);
        scaleAnimation.setFillEnabled(true);
        scaleAnimation.setFillAfter(true);
        view.startAnimation(scaleAnimation);
    }

    public static void extendWidth(final View view, final int toWidth, final int toHeight, int duration, Animation.AnimationListener animationListener) {
        Animation extendAnimation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                float x = view.getX();
                int currentWidth = view.getWidth() + (int) ((toWidth - view.getWidth())  * interpolatedTime);
                int currentHeight = view.getHeight() + (int) ((toHeight - view.getHeight())  * interpolatedTime);
                view.getLayoutParams().width = currentWidth;
                view.getLayoutParams().height = currentHeight;

                view.requestLayout();
                view.setX(x);

            }
        };
        extendAnimation.setDuration(duration);
        extendAnimation.setFillEnabled(true);
        extendAnimation.setFillAfter(true);
        extendAnimation.setAnimationListener(animationListener);
        view.startAnimation(extendAnimation);
    }
}
