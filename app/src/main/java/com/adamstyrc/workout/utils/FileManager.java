package com.adamstyrc.workout.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.adamstyrc.workout.model.Exercise;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by adamstyrc on 29/12/15.
 */
public class FileManager {

    private File root;

    public FileManager(Context context) {
//        root = context.getFilesDir();
        root = context.getExternalMediaDirs()[0];
    }

    public void download(String fileUrl) throws IOException {
        root.mkdirs();

        String fileName = Utils.extractFilenameFromUrl(fileUrl);
        File file = new File(root, fileName);

        download(fileUrl, file.getPath());
    }

    public void download(String fileUrl, String destinationPath) throws IOException {
        String tempDestinationPath = destinationPath + "_tmp";

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(fileUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return;
//                return "Server returned HTTP " + connection.getResponseCode()
//                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(tempDestinationPath);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                total += count;
                // publishing the progress....
//                if (fileLength > 0) // only if total length is known
//                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

            File tempFile = new File(tempDestinationPath);
            File finalFile = new File(destinationPath);
            tempFile.renameTo(finalFile);

        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
    }

    public boolean isFileDownloaded(Exercise exercise) {
        if (!TextUtils.isEmpty(exercise.getLocalVideoRawName())) {
            return true;
        } else{
            String filename = Utils.extractFilenameFromUrl(exercise.getVideoUrl());
            File file = new File(root, filename);

            Log.d("ACHTUNG", "isFileDownloaded " + file.exists());
            return file.exists();
        }
    }

    public String getFilePath(String fileUrl) {
        String filename = Utils.extractFilenameFromUrl(fileUrl);
        File file = new File(root, filename);
        return file.getAbsolutePath();
    }

    public File getFile(String fileUrl) {
        String filename = Utils.extractFilenameFromUrl(fileUrl);
        return new File(root, filename);
    }
}
