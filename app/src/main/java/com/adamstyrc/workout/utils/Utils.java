package com.adamstyrc.workout.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;

import java.io.File;

/**
 * Created by adamstyrc on 19/12/15.
 */
public class Utils {

    private Utils() {}

    public static Uri getUriForRawResource(Context context, String localVideoRawName) {
        return Uri.parse("android.resource://" + context.getPackageName() + "/raw/"
                + localVideoRawName);
    }

    public static boolean isSdcardAvailable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static long getAvailableSdcardSpace() {
        File extdir = Environment.getExternalStorageDirectory();
        StatFs statFs = new StatFs(extdir.getAbsolutePath());
        long availableBytes = statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong();
        return availableBytes;
    }

    public static String extractFilenameFromUrl(String url) {
        String originalFilename = url.substring(url.lastIndexOf('/') + 1, url.length());
        originalFilename = originalFilename.replace("+", "_");
        originalFilename = originalFilename.replace(" ", "_");
        return originalFilename;
    }
}
