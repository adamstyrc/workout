package com.adamstyrc.workout.analytics;

import android.content.Context;

import com.adamstyrc.workout.R;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by adamstyrc on 23/01/16.
 */
public class GoogleAnalytics {
    private static Tracker sTracker;

    synchronized public static Tracker getDefaultTracker(Context context) {
        if (sTracker == null) {
            com.google.android.gms.analytics.GoogleAnalytics analytics = com.google.android.gms.analytics.GoogleAnalytics.getInstance(context.getApplicationContext());
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            sTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return sTracker;
    }
}
